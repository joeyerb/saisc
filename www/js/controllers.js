angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('StudertTermCtrl', function($scope, $http) {
    host_url = 'http://ror.zjut.party:3000';
    
    $scope.info = {student_id: 1, term: '201402'}
    $scope.doQuery = function() {
        $http.get(host_url + '/classes/1/students/' + $scope.info.student_id + '/term/' + $scope.info.term + '.json')
            .success(function(data) {
                $scope.courses = data;
            });
    }
})

.controller('StudertRankCtrl', function($scope, $http) {
    host_url = 'http://ror.zjut.party:3000';
    
    $scope.info = {major_id: 1, grade: '2013'}
    $scope.doQuery = function() {
        $http.get(host_url + '/majors/' + $scope.info.major_id + '/rank/' + $scope.info.grade + '.json')
            .success(function(data) {
                $scope.students = data;
            });
    }
})

.controller('StudertCoursesCtrl', function($scope, $http) {
    host_url = 'http://ror.zjut.party:3000';
    
    $scope.info = {student_id: 1}
    $scope.doQuery = function() {
        $http.get(host_url + '/classes/1/students/' + $scope.info.student_id + '/courses.json')
            .success(function(data) {
                $scope.courses = data;
            });
    }
})

.controller('StudertScoresCtrl', function($scope, $http) {
    host_url = 'http://ror.zjut.party:3000';
    
    $scope.info = {student_id: 1}
    $scope.doQuery = function() {
        $http.get(host_url + '/classes/1/students/' + $scope.info.student_id + '/scores.json')
            .success(function(data) {
                $scope.courses = data;
            });
    }
})

.controller('CourseScoresCtrl', function($scope, $http) {
    host_url = 'http://ror.zjut.party:3000';
    
    $scope.info = {course_id: 1, grade: '2013'}
    $scope.doQuery = function() {
        $http.get(host_url + '/courses/' + $scope.info.course_id + '/grade/' + $scope.info.grade + '.json')
            .success(function(data) {
                $scope.students = data;
                var sumscore = 0
                for (var i in data)
                    sumscore += data[i].score;
                $scope.avgscore = "平均成绩：" + (sumscore/data.length).toString();
            });
    }
})

.controller('TeacherCoursesCtrl', function($scope, $http) {
    host_url = 'http://ror.zjut.party:3000';
    
    $scope.info = {teacher_id: 1}
    $scope.doQuery = function() {
        $http.get(host_url + '/teachers/' + $scope.info.teacher_id + '/courses.json')
            .success(function(data) {
                $scope.courses = data;
            });
    }
})

.controller('ClassCoursesCtrl', function($scope, $http) {
    host_url = 'http://ror.zjut.party:3000';
    
    $scope.info = {class_id: 1}
    $scope.doQuery = function() {
        $http.get(host_url + '/class_courses.json')
            .success(function(data) {
                $scope.courses = data;
            });
    }
})

.controller('AddScoreCtrl', function($scope, $http) {
    host_url = 'http://ror.zjut.party:3000';
    
    $scope.info = {}
    $scope.doUpdate = function() {
        $http({
            method: 'POST',
            url: host_url + '/reports.json',
            data: { 'xucf_report': $scope.info }
        });
    }
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
