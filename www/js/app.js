// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.student', {
    url: '/student',
    views: {
      'menuContent': {
        templateUrl: 'templates/student.html'
      }
    }
  })
    .state('app.student-term', {
      url: '/student/term',
      views: {
        'menuContent': {
          templateUrl: 'templates/student_term.html',
          controller: 'StudertTermCtrl'
        }
      }
    })
    .state('app.student-rank', {
      url: '/student/rank',
      views: {
        'menuContent': {
          templateUrl: 'templates/student_rank.html',
          controller: 'StudertRankCtrl'
        }
      }
    })
    .state('app.student-courses', {
      url: '/student/courses',
      views: {
        'menuContent': {
          templateUrl: 'templates/student_courses.html',
          controller: 'StudertCoursesCtrl'
        }
      }
    })
    .state('app.student-scores', {
      url: '/student/scores',
      views: {
        'menuContent': {
          templateUrl: 'templates/student_scores.html',
          controller: 'StudertScoresCtrl'
        }
      }
    })

    .state('app.course-scores', {
      url: '/course/scores',
      views: {
        'menuContent': {
          templateUrl: 'templates/course_scores.html',
          controller: 'CourseScoresCtrl'
        }
      }
    })
  .state('app.teacher-courses', {
      url: '/teacher/courses',
      views: {
        'menuContent': {
          templateUrl: 'templates/teacher_courses.html',
          controller: 'TeacherCoursesCtrl'
        }
      }
    })
    .state('app.class-courses', {
      url: '/class/courses',
      views: {
        'menuContent': {
          templateUrl: 'templates/class_courses.html',
          controller: 'ClassCoursesCtrl'
        }
      }
    })
  .state('app.addscore', {
    url: '/addscore',
    views: {
      'menuContent': {
        templateUrl: 'templates/addscore.html',
        controller: 'AddScoreCtrl'
      }
    }
  })

  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/student');
});
